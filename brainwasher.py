#!/usr/bin/python2
#coding=utf8
import sys
import random
import os

def main(argv):
    if not argv:
        stdintext = []
        for lines in sys.stdin:
            if lines[0] not in ["#", "＃", "\n", "\r\n"]:
                lines = lines.replace('　', ' ')
                stdintext.append(lines.split(' ', 2))
        BeginBrainwashing(stdintext)
    elif argv[0] in ["-h", "--help"]:
        print "Brainwasher 0.1"
        print "<https://bitbucket.org/Kresna/brainwasher>\n"
        print "A small script to assit in rote learning."
        print "Accepts either a file or a pipe\n"
        print "eg:"
        print "  brainwasher.py ./mywords.txt"
        print "  This will use the words in ./mywords.txt\n"
        print "  cat ./mywords1.txt ./mywords2.txt | brainwasher.py"
        print "  This will read from the pipe, which is a concatenation of"
        print "  mywords1.txt and mywords2.txt\n"
        print "Word File layout"
        print "The file containing words has a simple structure. # denotes a comment,"
        print "blank lines are acceptable, and single or double wide spaces are OK."
        print "The structure of an entry is WORD READING MEANING."
        print " - WORD must be one word with no spaces."
        print " - READING must consist of no spaces."
        print " - MEANING can have as many words or spaces as desired."
    else:
        wordlist = []
        textfile = open(argv[0], 'r')
        for lines in textfile:
            if lines[0] not in ["#", "＃", "\n", "\r\n"]:
                lines = lines.replace('　', ' ')
                wordlist.append(lines.split(' ', 2))
        BeginBrainwashing(wordlist)

def BeginBrainwashing(wordlist):
    while True:
        random.shuffle(wordlist)
        for entry in range(len(wordlist)):
            print "Word: {0}".format(wordlist[entry][0])
            if os.name == "posix":
                sys.stdin = open('/dev/tty')
            elif os.name == "nt":
                sys.stdin = open('CON')
            answer = raw_input(": ")
            if answer == wordlist[entry][1]:
                print "\n\n"
                print "Correct"
                print "Word: {0}   Reading: {1}   Meaning: {2}".format(wordlist[entry][0], wordlist[entry][1], wordlist[entry][2])
                print "\n\n"
            else:
                print "\n\n"
                print "Incorrect"
                print "Word: {0}   Reading: {1}   Meaning: {2}".format(wordlist[entry][0], wordlist[entry][1], wordlist[entry][2])
                raw_input("Press enter to continue...")
                print "\n"

if __name__ == "__main__":
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print "\n"
        pass
