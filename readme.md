#Brainwasher

##Built in help
```
Brainwasher 0.1
<https://bitbucket.org/Kresna/brainwasher>

A small script to assit in rote learning.
Accepts either a file or a pipe

eg:
  brainwasher.py ./mywords.txt
  This will use the words in ./mywords.txt

  cat ./mywords1.txt ./mywords2.txt | brainwasher.py
  This will read from the pipe, which is a concatenation of
  mywords1.txt and mywords2.txt

Word File layout
The file containing words has a simple structure. # denotes a comment,
blank lines are acceptable, and single or double wide spaces are OK.
The structure of an entry is WORD READING MEANING.
 - WORD must be one word with no spaces.
 - READING must consist of no spaces.
 - MEANING can have as many words or spaces as desired.
```
